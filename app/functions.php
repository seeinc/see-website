<?php

require_once('i18next.php');

static $title;
static $language;
static $pageTitleBase = "SEE";

session_start();

try {
	$sessionLang = isset($_SESSION["lang"]) ? $_SESSION["lang"] : '';
	$getLang = isset($_GET["lang"]) ? $_GET["lang"] : '';

	if ($getLang != "") {
		$_SESSION["lang"] = $_GET["lang"];
		$language = $_GET["lang"];
	}
	elseif ($sessionLang != "") {
		$language = $_SESSION["lang"];
	}
	else {
		$language = "pl"; // default
	}
	// echo 'Lang: ' . $language;
	i18next::init($language, 'languages/'. $language .'.json');
} catch (Exception $e) {
	echo 'Caught exception: ' . $e->getMessage();
}

function t($key, $variables = array()) {
	return i18next::getTranslation($key, $variables);
}

function echoPageTitle() {
	global $pageTitleBase, $pageTitle, $title;

	echo $pageTitleBase;
	if ($pageTitle != "") {
		// strip out all whitespace
		$title = preg_replace('/\s*/', '', $pageTitle);
		// convert the string to all lowercase
		$title = strtolower($title);
		echo " – " . t($title . '.pageTitle');
		// echo $pageTitle;
	}
	else {
		$title = 'home';
		echo " – " . t('home.pageTitle'); 
	}
}

function echoPageDescription() {
	global $title;

	echo t($title . '.pageDescription');
}

function echoBodyClasses() {
	global $pageTitle, $title;
	$classes = "";
	if ($pageTitle == "") {
		echo ' id="page-home"';
	}
	else {
		echo ' id="page-'.$title.'"';
		$classes = $classes." not-body";
	}
	echo ' class="'.$classes.'"';
}

?>