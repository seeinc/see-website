    <div id="topbar-container" class="">
      <div id="topbar">
        <div class="">
          <!-- <a id="logo" href="#hero"> -->
          <a id="logo" href="https://www.see.inc">
            <picture>
              <source srcset="images/logo.webp" type="image/webp">
              <img src="images/logo.png" class="logo" alt="SEE">
            </picture>
          </a>
          <button id="nav-icon">
            <span></span><span></span><span></span><span></span>
          </button>
        </div>
        <nav class="navigation section-nav">
          <ul class="main-menu menu">
            <?php /* <li><a href="./index.php"><?php echo t('menu.home'); ?></a></li>
            <li><a href="./careers.php"><?php echo t('menu.career'); ?></a></li>
            <!-- <li><a href="#hero"><?php echo t('menu.about'); ?></a></li> -->
            */?>
            <li><a href="#vision"><?php echo t('menu.vision'); ?></a></li>
            <li><a href="#robots"><?php echo t('menu.robots'); ?></a></li>
            <li><a href="#portal"><?php echo t('menu.portal'); ?></a></li>
            <li><a href="#framework"><?php echo t('menu.framework'); ?></a></li>
            <li><a href="#careers"><?php echo t('menu.careers'); ?></a></li>
            <li><a href="#contact"><?php echo t('menu.contact'); ?></a></li>
          </ul>
        </nav>
      </div>
    </div>