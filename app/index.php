<?php
// Page params
$pageTitle = "";
$showScrollDown = true;

require_once("header.php");

?>
<!-- Primary Page Layout
––––––––––––––––––––––––––––––––––––––––––––––––––-->
<section id="hero" class="image-section">
  <div class="container">
    <h1><?php echo t('home.heroTitle'); ?></h1>
    <a href="#contact">
      <button class="button-primary"><?php echo t('home.heroButton'); ?></button>
    </a>
  </div>
</section>
<section id="vision" class="image-section">
  <div class="container">
    <h2><?php echo t('home.sections.visionTitle'); ?></h2>
    <p><?php echo t('home.sections.visionDescription'); ?></p>
  </div>
</section>
<section id="robots" class="image-section">
  <div class="container">
    <h2><?php echo t('home.sections.robotsTitle'); ?></h2>
    <p><?php echo t('home.sections.robotsDescription'); ?></p>
  </div>
</section>
<section id="portal" class="image-section">
  <div class="container">
    <h2><?php echo t('home.sections.portalTitle'); ?></h2>
    <p><?php echo t('home.sections.portalDescription'); ?></p>
  </div>
</section>
<section id="framework" class="image-section">
  <div class="container">
    <h2><?php echo t('home.sections.frameworkTitle'); ?></h2>
    <p><?php echo t('home.sections.frameworkDescription'); ?></p>
  </div>
</section>
<section id="careers" class="image-section">
  <div class="container">
    <h2><?php echo t('home.sections.careersTitle'); ?></h2>
    <?php require_once("careers.php"); ?>
  </div>
</section>
<section id="contact" class="image-section">
  <?php require_once("contact.php"); ?>
  <button class="close"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/></svg></button>
</section>

<?php require_once("footer.php"); ?>