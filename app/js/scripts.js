window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;

  // 20 is an arbitrary number here, just to make you think if you need the prevScrollpos variable:
  if (currentScrollPos > 20) {
    document.getElementById("ic-scroll").classList.add("scrolled");
    document.getElementById("footer").classList.add("scrolled");
  }
  else {
    document.getElementById("ic-scroll").classList.remove("scrolled");
    document.getElementById("footer").classList.remove("scrolled");
  }
  if (currentScrollPos > 500) {
  	document.getElementById("footer").classList.remove("scrolled");
  }
}

/**
 *	jQuery scripts
 */
$(document).ready(function($){ 

/**
 *	Full page side nav v1.0
 */
var sectionLinks = $('a[href^=\\#]');
var contactClose = $('#contact button.close');
var menuButton = $('#nav-icon');
var desktop = false;


sectionLinks.click(function(event) {
	if ($(window).width()>1100) {
		desktop = true;
	}
	else {
		desktop = false;
	}
	var id = $(this).attr("href");
	// var target = $(id).offset().top-200;
	if (menuButton.hasClass("open")) {
		menuButton.removeClass("open");
		$('#topbar-container').removeClass("open");
		$('body').removeClass("noscroll");
	}
	// if (id == "#contact" && desktop) {
	// 	if ($(id).hasClass("active")) { // Contact opened
	// 		$(id).removeClass("active");
	// 		$('a[href=#contact]').removeClass("active");
	// 	}
	// 	else {
	// 		sectionLinks.removeClass("active");
	// 		$(id).addClass("active");
	// 		setTimeout(function(){
	// 			$('body').addClass("noscroll");
	// 		}, 300);
	// 	}
	// }
	// else {
		// if ($('#contact').hasClass("active")) { // Contact opened, menu visible
		// 	$('a[href^=\\#contact]').removeClass("active");
		// 	$('#contact').removeClass("active");
		// }
		var target = $(id).offset().top;
		$('html').animate({scrollTop:target}, 500);
	// }
	isSelected($(window).scrollTop());
	event.preventDefault();
});

// contactClose.click(function(event) {
// 	if ($('#contact').hasClass("active")) {
// 		$('a[href^=\\#contact]').removeClass("active");
// 		$('#contact').removeClass("active");
// 		$('body').removeClass("noscroll");
// 	}
// });

menuButton.click(function(event) { // wip
	if (menuButton.hasClass("open")) { // is opened
		menuButton.removeClass("open");
		$('#topbar-container').removeClass("open");
		$('body').removeClass("noscroll");
	}
	else { // is closed
		menuButton.addClass("open");
		$('#topbar-container').addClass("open");
		setTimeout(function(){
			$('body').addClass("noscroll");
		}, 300);
	}
});

function getTargetTop(elem){
	var id = elem.attr("href");
	var offset = 0;
	return $(id).offset().top - offset;
}

function isSelected(scrolledTo){
	var threshold = 500;
	var i;

	for (i = 0; i < sectionLinks.length; i++) {
		var section = $(sectionLinks[i]);
		// if (section.attr("href")=="#contact") {

		// }
		// else {
			var target = getTargetTop(section);

			if (scrolledTo > target - threshold && scrolledTo < target + threshold) {
				sectionLinks.removeClass("active");
				section.addClass("active");
			}
		// }

	};
}

/* Scroll, sticky topbar */
var topPosition = 0;
 
$(window).scroll(function() {
    var scrollMovement = $(window).scrollTop();
   
    if (topPosition < $(window).height()*0.18 ){
    	$('#topbar-container').removeClass('sticky');
		$('#footer').addClass('sticky');
    }
    else{
		$('#topbar-container').addClass('sticky');
		$('#footer').removeClass('sticky');
    }
    topPosition = scrollMovement;

	isSelected($(window).scrollTop());
});


});
/**
 *	End of jQuery scripts
 */