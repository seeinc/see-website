<?php
/**
 * The header of SEE website
 * This file should be included to every page once.
*/

require_once("functions.php");

?>
<!DOCTYPE html>
<html lang="<?php echo $language; ?>">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title><?php echoPageTitle(); ?></title>
  <meta name="description" content="<?php echoPageDescription(); ?>">
  <meta name="author" content="Source Enterprise">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--build:css css/styles.min.css-->
  <link rel="stylesheet" href="css/style.css">
  <!--endbuild-->

  <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>
  <script type="text/javascript" src="js/require.js"></script>
  <script type="text/javascript" src="js/modernizr-custom.js"></script>

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#101820">
  <link rel="shortcut icon" href="/favicon.ico">
  <meta name="msapplication-TileColor" content="#101820">
  <meta name="theme-color" content="#ffffff">

  <link rel="canonical" href="https://see.inc/" />

</head>

<body<?php echoBodyClasses(); ?>>
<?php require_once("menu.php"); ?>

  <div id="content">