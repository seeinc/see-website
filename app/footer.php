	</div>
	<footer id="footer" class="sticky">
		<div class="column">Copyright © <?php echo date("Y") . " " . t('copyright'); ?></div>
		<?php /* <nav class="navigation">
		  <ul class="language-menu menu">
		    <li><a href="?lang=en">EN</a></li>
		    <li><a href="?lang=pl">PL</a></li>
		  </ul>
		</nav> */ ?>
	</footer>

<?php
if ($showScrollDown) { ?>
	<div id="ic-scroll">
		<span class="ic-scroll-arrow first"></span>
		<span class="ic-scroll-arrow second"></span>
		<span class="ic-scroll-arrow third"></span>
	</div>
<?php } ?>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
